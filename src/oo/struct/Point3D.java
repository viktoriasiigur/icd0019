package oo.struct;

public class Point3D {
    public Integer z;
    public Integer x;
    public Integer y;

    public Point3D(Integer x, Integer y, Integer z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
