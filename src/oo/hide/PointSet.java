package oo.hide;

public class PointSet {

    private int pointSetIndex = 0;
    private Point[] pointsArray;


    public PointSet(Integer initialCapacity) {
        pointsArray = new Point[initialCapacity];

    }

    public PointSet() {
        pointsArray = new Point[100];
    }

    public void add(Point point) {
        boolean canAdd = true;
        for (Point p : pointsArray) {
            if (point.equals(p)) {
                canAdd = false;
            }
        }
        if (canAdd) {
            if (pointSetIndex == pointsArray.length) {
                int index = 0;
                Point[] newPointsArray = new Point[pointsArray.length*2];
                for (Point oldPoint: pointsArray) {
                    newPointsArray[index] = oldPoint;
                    index += 1;
                }
                pointsArray = newPointsArray;
                pointsArray[pointSetIndex] = point;
                pointSetIndex += 1;
            }
            else {
                pointsArray[pointSetIndex] = point;
                pointSetIndex += 1;
            }
        }

    }

    public Integer size() {
        int counter = 0;
        for (Point p: pointsArray){
            if (p != null) {
                counter += 1;
            }
        }
        return counter;
    }

    public boolean contains(Point p) {
        boolean PointInPointsArray = false;
        for (Point point: pointsArray) {
            if (point != null) {
                if (point.equals(p)) {
                    PointInPointsArray = true;
                }
            }
        }
        return PointInPointsArray;
    }

    @Override
    public String toString() {
        String finalString = "";

        for (Point point: pointsArray) {
            if (point != null) {
                finalString += point.toString() + ", ";
            }
        }

        if (finalString.length() > 2) {
            finalString = finalString.substring(0, finalString.length() - 2);
        }

        return finalString;
    }
}
