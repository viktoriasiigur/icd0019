package collections.simulator;

import java.util.*;

public class Hand {


    public List<Card> hand = new ArrayList<>();


    public void addCard(Card card) {
        hand.add(card);
    }

    @Override
    public String toString() {
        List<String> cardsList = new ArrayList<>();
        for (Card card : hand) {
               cardsList.add(card.toString());
        }
        return "[" + String.join(", ", cardsList) + "]";
    }
    private Map cardsCounterMap() {
        Map<Integer, Integer> hashMap = new HashMap<>();
        for (Card card : hand) {
            Integer hash = card.hashCode();
            if (hashMap.containsKey(hash)) {
                hashMap.put(hash, hashMap.get(hash) + 1);
            } else {
                hashMap.put(hash, 1);
            }
        }
        return hashMap;
    }

    private int pairsCounter() {
        int pairs = 0;

        for (Object value : cardsCounterMap().values()) {
            if ((int) value == 2) {
                pairs += 1;
            }
        }
        return pairs;
    }

    private int tripsCounter() {
        int trips = 0;

        for (Object value : cardsCounterMap().values()) {
            if ((int) value == 3) {
                trips += 1;
            }
        }
        return trips;
    }



    public boolean isOnePair() {
        return pairsCounter() == 1 && tripsCounter() == 0;
    }

    public boolean isTwoPairs() {
        return pairsCounter() == 2;
    }

    public boolean isTrips() {
        return tripsCounter() == 1 && pairsCounter() == 0;
    }

    public boolean isFullHouse() {
        return (tripsCounter() == 1) && (pairsCounter() == 1);
    }


}
