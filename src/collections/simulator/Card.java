package collections.simulator;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Card implements Comparable<Card> {

    public enum CardValue { s2, s3, s4, s5, s6, s7, s8, s9, s10, J, Q, K, A }

    public enum CardSuit { C, D, H, S }

    private CardValue value;
    private CardSuit suit;

    public Card(CardValue value, CardSuit suit) {
        this.value = value;
        this.suit = suit;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Card)) {
            return false;
        }

        Card other = (Card) obj;

        return Objects.equals(value, other.value) && Objects.equals(suit, other.suit);
    }


    @Override
    public int compareTo(Card other) {
        List<Card.CardValue> cardValues = Arrays.asList(Card.CardValue.values());
        if (cardValues.indexOf(value) < cardValues.indexOf(other.value)) {
            return -1;
        } else if (cardValues.indexOf(value) > cardValues.indexOf(other.value)) {
            return 1;
        } else if (cardValues.indexOf(value) == cardValues.indexOf(other.value)) {
            return 0;
        }
        throw new IllegalStateException("something went wrong");
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", value, suit);
    }
}
