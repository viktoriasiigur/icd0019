package collections.simulator;

import java.util.*;

public class Simulator {

    private List<Card.CardValue> cardValues = Arrays.asList(Card.CardValue.values());
    private List<Card.CardSuit> cardSuits = Arrays.asList(Card.CardSuit.values());

    List<Card> deck = deckMaker();

    private List<Card> deckMaker()  {
        List<Card> deck = new ArrayList<>();
        int valueIndex = 0;
        int suitIndex = 0;
        for (int i = 0; i < 52; i++) {
            if (valueIndex == 13) {
                valueIndex = 0;
                suitIndex += 1;
            }
            if (suitIndex == 4) {
                suitIndex = 0;
            }

            Card card = new Card(cardValues.get(valueIndex), cardSuits.get(suitIndex));
            deck.add(card);
            valueIndex += 1;
        }
        return deck;
    }


    private Hand randomHand() {
        Hand hand = new Hand();
        Collections.shuffle(deck);
        int count = 0;
        for (Card card : deck) {
            hand.addCard(card);
            if (count == 4) {
                break;
            }
            count += 1;
        }

        return hand;
    }


    private int FirstCollision(HandType handtype) {
        if (handtype == HandType.PAIR) {
            for (int i = 0; i < 50; i++) {
                Hand randomHand = randomHand();
                if (randomHand.isOnePair()) {
                    return i + 1;
                }
            }
            throw new IllegalStateException("something is bad");

        } if (handtype == HandType.TWO_PAIRS) {
            for (int i = 0; i < 400; i++) {
                Hand randomHand = randomHand();
                if (randomHand.isTwoPairs()) {
                    return i + 1;
                }
            }
            throw new IllegalStateException("TW something is bad");

        } if (handtype == HandType.THREE_OF_A_KIND) {
            for (int i = 0; i < 2000; i++) {
                Hand randomHand = randomHand();
                if (randomHand.isTrips()) {
                    return i + 1;
                }
            }
            throw new IllegalStateException("TR something is bad");

        } if (handtype == HandType.FULL_HOUSE) {
            for (int i = 0; i < 15000; i++) {
                Hand randomHand = randomHand();
                if (randomHand.isFullHouse()) {
                    return i + 1;
                }
            }
            throw new IllegalStateException("FH something is bad");
        }
        throw new IllegalStateException("bad code");
    }


    private double Probability(HandType handType) {

        List<Integer> counts = new ArrayList<>();

        for (int i = 0; i < 150000; i++) {
            counts.add(FirstCollision(handType));
        }

        int sum = 0;
        for (Integer count: counts) {
            sum += count;
        }
        // System.out.println("done");
        return (double) counts.size() / sum * 100;

    }


    public Map<HandType, Double> calculateProbabilities() {
        Map<HandType, Double> probabilitiesMap = new HashMap<>();

        probabilitiesMap.put(HandType.PAIR, Probability(HandType.PAIR));

        // probabilitiesMap.put(HandType.TWO_PAIRS, Probability(HandType.TWO_PAIRS));

        // probabilitiesMap.put(HandType.THREE_OF_A_KIND, Probability(HandType.THREE_OF_A_KIND));

        // probabilitiesMap.put(HandType.FULL_HOUSE, Probability(HandType.FULL_HOUSE));

        System.out.println(probabilitiesMap);

        return probabilitiesMap;
    }


}
