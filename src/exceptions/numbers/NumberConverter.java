package exceptions.numbers;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class NumberConverter {

    private Properties properties;

    public NumberConverter(String lang) {

        String filePath = String.format("src/exceptions/numbers/numbers_%s.properties", lang);
        properties = new Properties();
        FileInputStream is = null;

        try {
            File file = new File(filePath);

            is = new FileInputStream(filePath);
            InputStreamReader reader = new InputStreamReader(is, Charset.forName("UTF-8"));

            if (file.length() < 3) {
                throw new MissingTranslationException();
            }
            if (file.length() > 3 && file.length() < 30) {
                throw new BrokenLanguageFileException();
            }

            properties.load(reader);

        } catch (IOException e) {
            throw new MissingLanguageFileException();
        }

    }

    public String numberInWords(Integer number) {

        String word = properties.getProperty(String.valueOf(number));
        String sfxTeen = properties.getProperty(String.format("teen"));
        String sfxTens = properties.getProperty(String.format("tens-suffix"));
        String tensAfterDelimiter = properties.getProperty(String.format("tens-after-delimiter"));
        String hundredBeforeDelimiter = properties.getProperty(String.format("hundreds-before-delimiter"));
        String sfxHundred = properties.getProperty(String.format("hundred"));

        int ones = number % 10;
        int tens = number / 10;
        int hundreds = number / 100;

        String tensInWords = properties.getProperty(String.valueOf(tens));
        String onesInWords = properties.getProperty(String.valueOf(ones));
        String hundredInWords =  properties.getProperty(String.valueOf(hundreds));
        String oneHundred = hundredInWords + hundredBeforeDelimiter + sfxHundred;


        if (number > 10 && number < 20 && word == null) {
            word = onesInWords + sfxTeen;
        }

        if (number >= 20 && number < 100 && word == null) {
            if (properties.getProperty(String.valueOf(number - ones)) != null) {
                tensInWords = properties.getProperty(String.valueOf(number - ones));
                word = tensInWords + tensAfterDelimiter + onesInWords;
            }

            else if (ones == 0) {
                word = tensInWords + sfxTens;
            }

            else{
                word = tensInWords + sfxTens + tensAfterDelimiter + onesInWords;
            }
        }

        if (number == 100) {
            word = oneHundred;
        }

        if (number > 100 && number < 110) {
            word = oneHundred + " " + onesInWords;
        }

        if (number == 110 || number == 120 || number == 130) {
            String irregular = properties.getProperty(String.valueOf(number - 100));

            if (irregular != null) {
                word = oneHundred + " " + irregular;
            }
            else {
                word = oneHundred + " " + properties.getProperty(String.valueOf((number - 100) / 10)) + sfxTens;
            }
        }

        if (number > 110 && number < 120) {
            String irregular = properties.getProperty(String.valueOf(number - 100));
            if (irregular != null) {
                word = oneHundred + " " + irregular;
            }
            else {
                word = oneHundred + " " + onesInWords + sfxTeen;
            }
        }

        if (number > 120 && number < 130) {
            String twenty = properties.getProperty(String.valueOf(number - ones - 100));
            if (twenty == null) {
                twenty = properties.getProperty(String.valueOf((number - 100) / 10)) + sfxTens;
            }

            word = oneHundred + " " + twenty + tensAfterDelimiter + onesInWords;
        }

        return word;


    }
}