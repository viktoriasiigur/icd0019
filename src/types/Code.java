package types;

public class Code {

    public static void main(String[] args) {

        Integer[] numbers = new Integer[] {1, 3, -2, 9};
        String str = "a22b";
        System.out.println(squareDigits(str));

    }

    public static Integer sum(Integer[] numbers) {
        Integer sum = 0;

        for (Integer number : numbers) {
            sum += number;
        }
        return sum;
    }

    public static Double average(Integer[] numbers) {
        Double sum = 0.0;

        for (Integer number : numbers) {
            sum += number;
        }
        return sum / numbers.length;
    }

    public static Integer minimumElement(Integer[] integers) {
        Integer min = null;

        for (Integer each : integers) {
            if (min == null) {
                min = each;
            }

            if (each < min) {
                min = each;
            }
        }

        return min;
    }

    public static String asString(Integer[] elements) {
        String result = "";

        for (Integer element : elements) {
            if (element != elements[elements.length - 1]) {
                result += (element + ", ");
            }
            else {
                result += element;
            }
        }
        return result;
    }

    public static String squareDigits(String s) {
        String result = "";

        for (char c: s.toCharArray()) {
            if (Character.isDigit(c)) {
                Integer i = (Integer.parseInt(Character.toString(c)) * (Integer.parseInt(Character.toString(c))));
                result += i;
            }
            else {
                result += c;
            }
        }
        return result;
    }
}
