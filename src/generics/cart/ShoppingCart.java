package generics.cart;

import java.util.ArrayList;
import java.util.List;


public class ShoppingCart<T extends CartItem> {

    public List <T> cart = new ArrayList<>();

    public void add(T item) {
        cart.add(item);
    }

    public void removeById(String id) {
        List <T> toRemove = new ArrayList<>();
        for (T item : cart) {
            if (item.getId().equals(id)) {
                toRemove.add(item);
            }
        }
        cart.removeAll(toRemove);
    }

    private boolean containsId(String id) {
        for (T item : cart) {
            if (item.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    public Double getTotal() {
        double total = 0;
        for (T item : cart) {
            total += item.getPrice();
        }
        return total;
    }

    public void increaseQuantity(String id) {
        throw new RuntimeException("not implemented yet");
    }

    public void applyDiscountPercentage(Double discount) {
    }

    public void cancelDiscounts() {
        throw new RuntimeException("not implemented yet");
    }

    public void addAll(List items) {
        throw new RuntimeException("not implemented yet");
    }
}
