package inheritance.analyser;

public class FlatTaxSalesAnalyser extends MainSalesAnalyser {

    public FlatTaxSalesAnalyser(SalesRecord[] records) {
        super(records);
    }

    @Override
    protected Double getTaxRate() {
        return 1.2;
    }

}
