package inheritance.analyser;

public class DifferentiatedTaxSalesAnalyser extends MainSalesAnalyser {

    public DifferentiatedTaxSalesAnalyser(SalesRecord[] records) {
        super(records);

    }
    @Override
    protected Double getTotalSales() {
        double total = 0.0;
        for (SalesRecord item : records) {
            if (item.hasReducedRate()) {
                total += (item.getProductPrice()) / 1.1 * item.getItemsSold();
            } else {
                total += (item.getProductPrice()) / 1.2 * item.getItemsSold();
            }
        }
        return total;
    }

}
