package inheritance.analyser;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public abstract class MainSalesAnalyser {

    protected final SalesRecord[] records;


    public static final Double TAX_RATE = 1.0;

    public MainSalesAnalyser(SalesRecord[] records) {
        this.records = records;
    }

    protected Double getTaxRate() {
        return TAX_RATE;
    }

    protected Double getTotalSales() {
        double total = 0.0;
        for (SalesRecord item : records) {
            total += (item.getProductPrice() / getTaxRate()) * item.getItemsSold();
        }
        return total;
    }

    public Double getTotalSalesByProductId(String id) {
        double totalSalesById = 0.0;
        for (SalesRecord item : records) {
            if (item.getProductId().equals(id)) {
                totalSalesById += item.getProductPrice() * item.getItemsSold();
            }

        }
        return totalSalesById / getTaxRate();
    }

    public String getIdOfMostPopularItem() {
        Map<String, Integer> map = new HashMap<>();
        for (SalesRecord item : records) {
            if (!map.containsKey(item.getProductId())) {
                map.put(item.getProductId(), item.getItemsSold());
            } else {
                map.put(item.getProductId(), (map.get(item.getProductId()) + item.getItemsSold()));
            }
        }
        return Collections.max(map.entrySet(), Map.Entry.comparingByValue()).getKey();
    }

    public String getIdOfItemWithLargestTotalSales() {
        Map<String, Integer> map = new HashMap<>();
        for (SalesRecord item : records) {
            if (!map.containsKey(item.getProductId())) {
                map.put(item.getProductId(), item.getItemsSold() * item.getProductPrice());
            } else {
                map.put(item.getProductId(), (map.get(item.getProductId()) + item.getItemsSold() * item.getProductPrice()));
            }
        }
        return Collections.max(map.entrySet(), Map.Entry.comparingByValue()).getKey();
    }
}
