package junit;

public class Code {

    public static boolean isSpecial(Integer number) {
        int remainder = number % 11;

        return remainder == 0 || remainder == 1;
    }

    public static Integer longestStreak(String input) {
        if (input.length() == 0) {
            return 0;
        }

        String[] characters = input.split("");

        String lastSymbol = characters[0];
        int streakLength = 0;
        int longestStreak = 0;
        for (String currentSymbol : characters) {

            if (currentSymbol.equals(lastSymbol)) {
                streakLength++;
            }
            else {
                streakLength = 1;
            }

            if (longestStreak < streakLength) {
                longestStreak = streakLength;
            }

            lastSymbol = currentSymbol;
            
        }

        return longestStreak;
    }

    public static Boolean arrayContainsSpecificInteger(Integer[] array, Integer integer) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(integer)) {
                return true;
            }
        }
        return false;
    }

    public static Integer[] appendElementToArray(Integer element, Integer[] array)  {
        Integer[] newArray = new Integer[array.length + 1];
        for (Integer i = 0; i < array.length; i++) {
            newArray[i] = array[i];
        }
        newArray[newArray.length - 1] = element;
        array = newArray;
        return array;
    }

    public static Integer[] removeDuplicates(Integer[] input) {
        Integer[] array = new Integer[] {};
        for (Integer integer : input) {
            if (!arrayContainsSpecificInteger(array, integer)) {
                array = appendElementToArray(integer, array);
            }
        }
        return array;
    }

    public static Integer sumIgnoringDuplicates(Integer[] integers) {
        Integer[] duplicatesRemoved = removeDuplicates(integers);
        Integer sum = 0;
        for (Integer integer : duplicatesRemoved) {
            sum += integer;
        }
        return sum;
    }

}
